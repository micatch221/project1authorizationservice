const {Datastore} = require('@google-cloud/datastore')
const express = require('express')
const cors = require('cors')

const app = express();
app.use(express.json())
app.use(cors())

const datastore = new Datastore()


//read get a single employee
// async function getEmployee(){
//     const key = datastore.key(['Employee',5642368648740864]);
//     const response = await datastore.get(key)
//     console.log(response[0])
// }


async function getEmployee(email){
    const query = datastore.createQuery('employee').filter('email','=',email)
    const [data,metaInfo] = await datastore.runQuery(query);
    console.log(data)
    console.log(metaInfo)
    console.log(data[0].password)
    if (data)
    {
        console.log("Employee Exists")
    }else{
        console.log("Employee does not exist")
    }
   
}
getEmployee('tester1@wed.com')

app.get('/users/:email/verify',async (req,res)=>{
    const query = datastore.createQuery('employee').filter('email','=',req.params.email);
    const [data,metaInfo] = await datastore.runQuery(query);
    if (data)
    {
        res.status(200).send("User exists")
    }else{
        res.status(404).send("No user found")
    }
})

app.listen(3000, ()=>console.log('Application started'))